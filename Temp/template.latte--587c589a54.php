<?php

use Latte\Runtime as LR;

/** source: template/template.latte */
final class Template587c589a54 extends Latte\Runtime\Template
{
	public const Source = 'template/template.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!doctype html>
<html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<head>
	<title>Prumery</title>
    <style>

p {
  margin: 0;
}

body {
  font: 30px    Inter, sans-serif;

  display: flex;
  align-items: center;
  justify-content: center;
  
  min-height: 100vh;
  padding: 30px;
  margin: 0;
  
  color: #224;
  background:
    url(https://512pixels.net/downloads/macos-wallpapers/10-12.jpg)
    center / cover no-repeat fixed;
}

.card {
  max-width: 1000px;
  min-height: 200px;
  flex-direction: column;
  justify-content: space-between;
  margin: 30px;

  max-width: 1000px;
  height: 300px;
  padding: 10px;

  border: 1px solid rgba(255, 255, 255, .25);
  border-radius: 20px;
  background-color: rgba(255, 255, 255, 0.45);
  box-shadow: 0 0 10px 1px rgba(0, 0, 0, 0.25);

  backdrop-filter: blur(15px);
}
    </style>
</head>
<body>
   <div class="card">
        <h4>1.roč. 1.pol.</h4>
	    <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer11) /* line 53 */;
		echo '</strong></p>
    </div>
    <div class="card">
        <h4>1.roč. 2.pol.</h4>
	    <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer12) /* line 57 */;
		echo '</strong></p>
    </div>
    <div class="card">
        <h4>2.roč. 1.pol.</h4>
        <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer21) /* line 61 */;
		echo '</strong></p>
    </div>
    <div class="card">
        <h4>2.roč. 2.pol.</h4>
        <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer22) /* line 65 */;
		echo '</strong></p>
    </div>
    <div class="card">
        <h4>3.roč. 1.pol.</h4>
        <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer31) /* line 69 */;
		echo '</strong></p>
    </div>
    <div class="card">
        <h4>3.roč. 2.pol.</h4>
        <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer32) /* line 73 */;
		echo '</strong></p>
    </div>
    <div class="card">
        <h4>4.roč. 1.pol.</h4>
        <p>průměr: <strong>';
		echo LR\Filters::escapeHtmlText($prumer41) /* line 77 */;
		echo '</strong></p>
    </div>
   
</body>
</html>';
	}
}
