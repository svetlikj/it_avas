<?php
require_once("vendor/autoload.php");
$latte = new Latte\Engine;
$latte->setTempDirectory('Temp');

$prumer = [0, 0, 0, 0, 0, 0, 0];

$myfile = fopen("stat.csv", "r") or die("Unable to open file!");

// Načtení dat ze souboru CSV
while (($myCsvFile = fgetcsv($myfile)) !== false) {
    // Ošetření, zda máme dostatek prvků v poli
    if (count($myCsvFile) >= 8) {
        for ($a = 1; $a < 8; $a++) {
            $prumer[$a - 1] += $myCsvFile[$a];
            
        }
    } else {
        // Pokud pole nemá dostatek prvků, můžeme zobrazit chybu
        echo "Chyba: Neplatný formát souboru CSV.";
        exit;
    }
    
}

fclose($myfile);

// Výpočet průměrů
for ($i = 0; $i < 7; $i++) {
    $prumer[$i] = round($prumer[$i] / 104, 2);
}

// Předání průměrů do šablony
$params = [
    'prumer11' => $prumer[0],
    'prumer12' => $prumer[1],
    'prumer21' => $prumer[2],
    'prumer22' => $prumer[3],
    'prumer31' => $prumer[4],
    'prumer32' => $prumer[5],
    'prumer41' => $prumer[6]
];

    echo $params['prumer11'];
    echo $params['prumer12'];
    echo $params['prumer21'];
    echo $params['prumer22'];
    echo $params['prumer31'];
    echo $params['prumer32'];
    echo $params['prumer41'];

    

// Vykreslení výstupu
echo $latte->render('template.latte', $params);
?>